import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data_pano: {}
  },
  getters: {
    get_data_pano(state){
      let pano = {}
      pano.lon = state.data_pano.lon;
      pano.lat = state.data_pano.lat;
      pano.x = state.data_pano.utm_x;
      pano.y = state.data_pano.utm_y;
      pano.utm_code = state.data_pano.utm_code;
      pano.utm_srid = state.data_pano.utm_srid;
      return pano
    }
  },
  mutations: {
    change_data_pano(state, data){
      state.data_pano = data 
    }
  },
  actions: {},
  modules: {},
});
